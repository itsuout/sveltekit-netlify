# clean svelte-kit windicss starter template with jest and netlify as default deploy option
---


## Using

1. degit this repo 
```bash
degit https://itsuout@bitbucket.org/itsuout/frontend-template.git <project-name>
```

2. initiate project
```
yarn add
```

3. install netlify adapter
```
yarn add -D @sveltejs/adapter-static@next
```

